<?php

use Silex\Application;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\WebProfilerServiceProvider;

use Igorw\Silex\ConfigServiceProvider;
use MoshiMoshi\Form\Extension\AppTypeExtension;

require_once '../vendor/autoload.php';

$app = new Application();
$environment = getenv('APP_ENV') ? : 'dev';
$config = __DIR__ . '/config/' . $environment . '.json';

if (!is_readable($config)) {
  $app->abort('500', 'Le fichier de configuration est manquant.');
}

$app
    ->register(new FormServiceProvider())
    ->register(new ValidatorServiceProvider())
    ->register(new DoctrineServiceProvider())
    ->register(new ServiceControllerServiceProvider())
    ->register(new TranslationServiceProvider())
    ->register(new TwigServiceProvider())
    ->register(new UrlGeneratorServiceProvider())
    ->register(new ConfigServiceProvider($config, array('root_path' => __DIR__ . '/../')));

$app['form.config'] = require_once __DIR__ . '/config/anwser.php';

if ($app['debug']) {
  $app->register(new WebProfilerServiceProvider());
}

$app['translator']->addLoader('xlf', new Symfony\Component\Translation\Loader\XliffFileLoader());
$app['translator']->addResource('xlf', __DIR__ . '/../vendor/symfony/validator/Symfony/Component/Validator/Resources/translations/validators.fr.xlf', 'fr', 'validators');
$app['form.extensions'] = $app->share($app->extend('form.extensions', function($extensions) use ($app) {
          $extensions[] = new AppTypeExtension($app);

          return $extensions;
        }));

return $app;
?>

<?php
/**
 * Created by PhpStorm.
 * User: otrinh
 * Date: 28/05/2015
 * Time: 17:08
 */

return array(
    "answer1" => array(
        "id" => 1,
        "name" => "answer1",
        'title' => '<p class="big">Où <br>en êtes<br> vous</p><p>dans votre projet ?</p><p class="pagination">1/4</p>',
        'options' => array(
            'a' => array(
                'image' => 'images/question-1/1.jpg',
                'content' => '<p>En pleine ...</p>                        <p class=big>réflexion</p>                        <p>surtout quand vous partez travailler</p>',
            ),
            'b' => array(
                'image' => 'images/question-1/2.jpg',
                'content' => '<p>Au stade de la ...</p>                        <p class=big>maturité</p>                        <p>sur votre projet d\'entreprise</p>',
            ),
            'c' => array(
                'image' => 'images/question-1/3.jpg',
                'content' => '<p>Aujourd\'hui, c\'est ...</p>                   <p class=big>le grand saut !</p>                        <p>Les cartes de visites sont prêtes</p>',
            )
        ),
        'next' => 'answer2'
    ),
    "answer2" => array(
        "id" => 2,
        "name" => "answer2",
        'title' => '<p class=big>Comment<br>imaginez<br>-<br>vous</p> <p>votre entreprise ?</p> <p class=pagination>2/4</p>',
        'options' => array(
            'a' => array(
                'image' => 'images/question-2/1.jpg',
                'content' => '<p>Je gère</p> <p>tout</p> <p class=big>moi-même !</p>',
            ),
            'b' => array(
                'image' => 'images/question-2/2.jpg',
                'content' => '<p>Une</p> <p class=big>PME</p> <p>à taille humaine</p>',
            ),
            'c' => array(
                'image' => 'images/question-2/3.jpg',
                'content' => '<p>Des bureaux</p> <p>à travers</p> <p class=big>le monde</p>',
            )
        ),
        'next' => 'answer3'
    ),
    "answer3" => array(
        "id" => 3,
        "name" => "answer3",
        'title' => '<p class=big>Votre<br>Leitmotiv</p> <p>...</p> <p class=pagination>3/4</p>',
        'options' => array(
            'a' => array(
                'image' => 'images/question-3/1.jpg',
                'content' => '<p class=big>Surprenez</p>',
            ),
            'b' => array(
                'image' => 'images/question-3/2.jpg',
                'content' => "<p>Le temps,</p> <p>c'est de</p> <p class=big>l'argent</p>",
            ),
            'c' => array(
                'image' => 'images/question-3/3.jpg',
                'content' => '<p>Vers l\'infini et</p> <p class=big>au-delà !</p>',
            )
        ),
        'next' => 'answer4'
    ),
    "answer4" => array(
        "id" => 4,
        "name" => "answer4",
        'title' => '<p class=big>Le<br>Monde du<br> futur ?</p> <p class=pagination>4/4</p>',
        'options' => array(
            'a' => array(
                'image' => 'images/question-4/1.jpg',
                'content' => '<p class=big>Eco-responsable,</p> <p>pour une</p> <p>meilleure planète !</p>',
            ),
            'b' => array(
                'image' => 'images/question-4/2.jpg',
                'content' => '<p>Recentré</p> <p>autour de</p> <p class=big>la famille</p>',
            ),
            'c' => array(
                'image' => 'images/question-4/3.jpg',
                'content' => '<p class=big>Connecté</p> <p>à tout moment</p>',
            )
        ),
        'next' => ''
    )
);
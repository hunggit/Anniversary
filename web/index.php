<?php

use MoshiMoshi\Provider\ControllerServiceProvider;

$app = require '../app/app.php';
$ControllerServiceProvider = new ControllerServiceProvider();
$app
    ->register($ControllerServiceProvider)
    ->mount('/', $ControllerServiceProvider);

$app->run();
<?php

namespace MoshiMoshi\Controller;

use Silex\Application;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use Doctrine\DBAL\DBALException;
use Symfony\Component\HttpFoundation\Response;

class MainController
{
    public function indexAction(Application $app, Request $request)
    {
        $sqlGifts = 'SELECT * FROM gift';
        $gifts = $app['db']->fetchAll($sqlGifts);

        $form = $app['form.factory']->createBuilder('create')->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $date = new \DateTime();
            $date = $date->format('Y-m-d H:i:s');

            try {
                $app['db']->insert('gift', array(
                    'name' => $data['name'],
                    'created_at' => $date,
                ));
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            }
            //return $app['twig']->redirect('index.twig')
        }

        return $app['twig']->render('index.twig', array(
            'form' => $form->createView(),
            'gifts' => $gifts,
        ));
    }

    public static function sanitize($word)
    {
        return strip_tags(trim($word));
    }
}
<?php

namespace MoshiMoshi\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\DBAL\Connection;

class CreateType extends AbstractType
{
    /**
     * @var array
     */
    private $gift;

    public function __construct($gift)
    {
        $this->gift = $gift;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array(
            'attr' => array(
                'placeholder' => 'Nom du cadeau'
            ),
            'required' => true,
        ));

        /*$builder->add('optin', 'checkbox', array(
            'label' => 'Je souhaite recevoir les informations de la Communauté des Créateurs',
            'required' => false,
            'attr'     => array('checked' => 'checked'),
        ));*/

        /*$builder->add('profile', 'choice', array(
            'choices' => array(
                'a' => 'profile1',
                'b' => 'profile2',
                'c' => 'profile3',
            ),
            'label' => false,
        ));*/

        /*foreach ($this->steps as $index => $step) {
            $builder->add($index, 'choice', array(
                'choices' => array_flip(array_keys($step['options']))
            ));
        }*/
    }

    public function getName()
    {
        return 'create';
    }

}

?>

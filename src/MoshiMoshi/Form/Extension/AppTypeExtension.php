<?php

namespace MoshiMoshi\Form\Extension;

use MoshiMoshi\Form\Type\CreateType;
use Silex\Application;
use Symfony\Component\Form\AbstractExtension;
use MoshiMoshi\Form\Type\IndexType;

class AppTypeExtension extends AbstractExtension {

  /**
   * @var Application
   */
  private $app;
  
  /**
   * @param \Silex\Application
   */
  public function __construct(Application $app) {
    $this->app = $app;
  }

  protected function loadTypes() {
    return array(
      new CreateType(isset($this->app['form.config']) ? $this->app['form.config'] : array())
    );
  }

}

?>

<?php

namespace MoshiMoshi\Provider;

use MoshiMoshi\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ServiceProviderInterface;

class ControllerServiceProvider implements ControllerProviderInterface, ServiceProviderInterface {
  
  /**
   * @param \Silex\Application $app
   */
  public function boot(Application $app) {
    
  }
  
  /**
   * @param \Silex\Application $app
   */
  public function register(Application $app) {
    $app['controller.maincontroller'] = $app->share(function () use ($app) {
      return new Controller\MainController();
    });
  }
  
  /**
   * @param \Silex\Application $app
   * @return \Silex\Application
   */
  public function connect(Application $app) {
    /** @var \Silex\ControllerCollection $controllers */
    $controllers = $app['controllers_factory'];
    $controllers
        ->match('/', 'controller.maincontroller:indexAction')
        ->bind('homepage');
    return $controllers;
  }
}
?>
